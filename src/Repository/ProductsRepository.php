<?php

namespace App\Repository;

use App\Entity\Categories;
use App\Entity\Page;
use App\Entity\Products;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Products::class);
    }

    public function findAllProductsByModelCategory(int $category)
    {
        $products = $this->createQueryBuilder('p')
            ->join('p.model', 'm')
            ->addSelect('p')
            ->where('m.category = :category')
            ->setParameter('category', $category)
            ->orderBy('p.id', 'DESC')
            ->getQuery()->getResult();

        return $products;
    }

    public function findAllProductsBySearchQuery(string $query)
    {
        $products = $this->createQueryBuilder('p')
            ->join('p.model', 'm')
            ->join('m.category', 'c')
            ->join('p.characteristics', 'ch')
            ->addSelect('p')
            ->where('m.title LIKE :query')
            ->orWhere('c.title LIKE :query')
            ->orWhere('ch.color LIKE :query')
            ->orWhere('ch.sizes LIKE :query')
            ->orWhere('ch.other LIKE :query')
            ->setParameter('query', "%{$query}%")
            ->orderBy('p.id', 'DESC')
            ->getQuery()->getResult();
        return $products;
    }

    // /**
    //  * @return Page[] Returns an array of Page objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Page
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
