<?php
namespace App\Service;

class ConsoleLogger
{
    public static function log($message)
    {
        echo "[" . (new \DateTime())->format('Y-m-d H:i:s') . "] > {$message}" . PHP_EOL;
    }
}
