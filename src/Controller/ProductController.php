<?php
namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Products;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends PageController
{
    /**
     * @Route("/products", name="products")
     * @return Response
     */
    public function indexAction()
    {
        $products = $this->getDoctrine()->getRepository(Products::class)->findAll();
        return $this->render('products/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/category/{id}", name="category")
     * @param int $id
     * @return Response
     */
    public function categoriesAction(int $id)
    {
        $category = $this->getDoctrine()->getRepository(Categories::class)->find($id);
        $products = $this->getDoctrine()->getRepository(Products::class)
            ->findAllProductsByModelCategory($id);
        return $this->render('products/index.html.twig', [
            'products' => $products,
            'category' => $category,
        ]);
    }

    /**
     * @Route("/product/{id}", name="product")
     * @param int $id
     * @return Response
     */
    public function productAction(int $id)
    {
        $product = $this->getDoctrine()->getRepository(Products::class)->find($id);
        return $this->render('products/item.html.twig', [
            'product' => $product,
        ]);
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        $parameters['categories'] = $categories;
        return parent::render($view, $parameters, $response);
    }
}
