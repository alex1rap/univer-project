<?php

namespace App\Controller;

use App\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $pages = $this->getDoctrine()->getRepository(Page::class)->findBy(['isActive' => 1]);
        $parameters['pages'] = $pages;
        return parent::render($view, $parameters, $response);
    }
}
