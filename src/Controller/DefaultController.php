<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Products;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends PageController
{
    private $request;

    public function __construct(RequestStack $request){

        $this->request = $request;
    }

    /**
     * @Route("/", name="index")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render("default/index.html.twig", [
            'title' => "AirMax Products"
        ]);
    }

    /**
     * @param int $page
     * @Route("/news/{page}", name="news")
     * @return Response
     */
    public function newsAction(int $page = 1)
    {
        $posts = $this->getDoctrine()->getManager()->getRepository(Post::class)
            ->findBy([], ['updated' => 'DESC']);
        return $this->render("default/news.html.twig", [
            'title' => 'News',
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/article/{id}", name="article")
     * @param int $id
     * @return Response
     */
    public function articleAction(int $id)
    {
        $posts = $this->getDoctrine()->getManager()->getRepository(Post::class)
            ->findBy([], ['updated' => 'DESC']);
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        return $this->render("default/article.html.twig", [
            'post' => $post,
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/search", name="search")
     * @return Response
     */
    public function searchAction()
    {
        $query = $this->request->getCurrentRequest()->query->get('query');
        $products = $this->getDoctrine()->getRepository(Products::class)
            ->findAllProductsBySearchQuery($query);
        $news = $this->getDoctrine()->getRepository(Post::class)
            ->findAllNewsBySearchQuery($query);
        return $this->render("default/search.html.twig", [
            'query' => $query,
            'products' => $products,
            'news' =>$news,
        ]);
    }

}
