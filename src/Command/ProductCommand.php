<?php

namespace App\Command;

use App\Entity\Categories;
use App\Entity\Characteristics;
use App\Entity\Models;
use App\Entity\Products;
use App\Service\ConsoleLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ProductCommand extends Command
{
    private $em;

    public function __construct(?string $name = null, EntityManagerInterface $em = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    public function configure()
    {
        $this->setName('app:product:add')
            ->addArgument('category', InputArgument::REQUIRED, 'Category ID')
            ->addArgument('title', InputArgument::REQUIRED, 'Product title')
            ->addArgument('price', InputArgument::REQUIRED, 'Product price')
            ->addArgument('count', InputArgument::REQUIRED, 'Products count');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $pictureQuestion = new Question("Set the picture address:\n", null);
        $colorQuestion = new Question("Set the color name:\n", null);
        $speedQuestion = new Question("Set the speed in Mbps:\n", null);
        $sizesQuestion = new Question("Set the sizes in mm in format AxBxC:\n", null);
        $weightQuestion = new Question("Set the weight in grams:\n", null);
        $otherQuestion = new Question("Set the description (enter ':q' to next step):\n");

        $id = intval($input->getArgument('category'));
        $price = intval($input->getArgument('price'));
        $amount = intval($input->getArgument('count'));

        $picture = $helper->ask($input, $output, $pictureQuestion);
        $color = $helper->ask($input, $output, $colorQuestion);
        $speed = $helper->ask($input, $output, $speedQuestion);
        $sizes = $helper->ask($input, $output, $sizesQuestion);
        $weight = $helper->ask($input, $output, $weightQuestion);
        $other = $helper->ask($input, $output, $otherQuestion);

        $cmd = $other;
        while ($cmd != ':q') {
            $cmd = $helper->ask($input, $output, new Question("", ""));
            $other .= '<p>' . ($cmd != ':q' ? $cmd : '') . '</p>' . PHP_EOL;
        }

        $category = $this->em->getRepository(Categories::class)->find($id);

        ConsoleLogger::log("Creating new model...");
        $model = new Models();
        $model->setTitle($input->getArgument('title'));
        $model->setCategory($category);
        $model->setPicture($picture);

        ConsoleLogger::log("Done. Creating new characteristics...");
        $characteristics = new Characteristics();
        $characteristics->setColor($color);
        $characteristics->setSpeed($speed);
        $characteristics->setSizes($sizes);
        $characteristics->setWeight($weight);
        $characteristics->setOther($other);

        ConsoleLogger::log("Done. Creating new product...");
        $product = new Products();
        $product->setModel($model);
        $product->setCharacteristics($characteristics);
        $product->setPrice($price);
        $product->setAmount($amount);

        ConsoleLogger::log("Done. Saving to database...");
        $this->em->persist($model);
        $this->em->persist($characteristics);
        $this->em->persist($product);

        $this->em->flush();

        ConsoleLogger::log("Success.");
        return 0;
    }
}
