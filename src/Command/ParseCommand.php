<?php
namespace App\Command;

use App\Entity\Post;
use App\Service\ConsoleLogger;
use Doctrine\ORM\EntityManagerInterface;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCommand extends Command
{
    private static $URL = 'http://ubiquiti.net.ua/info';
    private $em;

    public function __construct(?string $name = null, EntityManagerInterface $em = null) {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setName('app:parse:html');
        $this->addArgument('id', InputArgument::REQUIRED, 'News ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = intval($input->getArgument('id'));
        $url = self::$URL . "?news_id={$id}";

        ConsoleLogger::log("Getting html from '{$url}'");
        $html = file_get_contents($url);

        ConsoleLogger::log("Done. Parsing...");
        $dom = HtmlDomParser::str_get_html($html);
        $title = $dom->find('h1')[0]->text();
        $content = implode(PHP_EOL, $dom->find('div.col-md-9 > p'));

        ConsoleLogger::log("Creating new post...");
        $post = new Post();
        $post->setTitle($title);
        $post->setContent($content);
        $post->setUpdated(new \DateTime());
        $this->em->persist($post);
        $this->em->flush();

        ConsoleLogger::log("All operations successfully done.");
        return 0;
    }
}
