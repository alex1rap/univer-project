<?php
namespace App\Command;

use App\Entity\Page;
use App\Service\ConsoleLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PageCommand extends Command
{
    private $em;

    public function __construct(?string $name = null, EntityManagerInterface $em = null) {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setName("app:page:add")
            ->addArgument('title', InputArgument::REQUIRED, 'Page title')
            ->addArgument('alias', InputArgument::REQUIRED, 'Default alias')
            ->addArgument('aliases', InputArgument::REQUIRED, 'Aliases');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $page = new Page();
        $page->setTitle($input->getArgument('title'));
        $page->setAlias($input->getArgument('alias'));
        $page->setAliases($input->getArgument('aliases'));
        $page->setIsActive(true);
        ConsoleLogger::log("Adding of new page ({$page->getTitle()})");
        $this->em->persist($page);
        $this->em->flush();
        ConsoleLogger::log("Success.");
        return 0;
    }

}
